extends Area2D


@export var experience_value: int = 1
@export var green_gem_value: int = 1
@export var green_gem_value_cap: int = 5
@export var blue_gem_value: int = 5
@export var blue_gem_value_cap: int = 25
@export var red_gem_value: int = 25
@export var speed: float = -1

var green_sprite := preload("res://assets/Textures/Items/Gems/Gem_green.png")
var blue_sprite := preload("res://assets/Textures/Items/Gems/Gem_blue.png")
var red_sprite := preload("res://assets/Textures/Items/Gems/Gem_red.png")

var target

@onready var sprite_2d: Sprite2D = %Sprite2D
@onready var collision_shape_2d: CollisionShape2D = %CollisionShape2D
@onready var collected_sound_player: AudioStreamPlayer = %CollectedSoundPlayer


func _ready() -> void:
	if experience_value < green_gem_value_cap:
		return
	elif experience_value < blue_gem_value_cap:
		sprite_2d.texture = blue_sprite
	else:
		sprite_2d.texture = red_sprite


func _physics_process(delta: float) -> void:
	if target:
		global_position = global_position.move_toward(target.global_position, speed)
		speed += 2 * delta


func collect() -> int:
	collected_sound_player.play()
	collision_shape_2d.set_deferred("disabled", true)
	sprite_2d.visible = false
	return experience_value


func _on_collected_sound_player_finished() -> void:
	queue_free()
