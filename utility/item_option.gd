extends Button


var item
@onready var player := get_tree().get_first_node_in_group("player")
@onready var name_label: Label = %NameLabel
@onready var description_label: Label = %DescriptionLabel
@onready var level_label: Label = %LevelLabel
@onready var item_icon: TextureRect = %ItemIcon


func _ready() -> void:
	grab_focus()
	if not item:
		item = "food"
	name_label.text = UpgradeDB.UPGRADES[item]["display_name"]
	description_label.text = UpgradeDB.UPGRADES[item]["details"]
	level_label.text = UpgradeDB.UPGRADES[item]["level"]
	item_icon.texture = load(UpgradeDB.UPGRADES[item]["icon"])
