class_name SpawnInfo
extends Resource


@export var time_start: float
@export var time_end: float
@export var enemy: Resource
@export var enemy_number: int
@export var enemy_spawn_delay: float

var spawn_delay_counter: float = 0.0
