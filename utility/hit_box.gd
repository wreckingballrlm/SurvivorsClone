extends Area2D


@export var damage: float = 1.0

@onready var collision_shape_2d: CollisionShape2D = %CollisionShape2D
@onready var disable_hit_box_timer: Timer = %DisableTimer


func temp_disable() -> void:
	collision_shape_2d.call_deferred("set", "disabled", true)


func _on_disable_timer_timeout() -> void:
	collision_shape_2d.call_deferred("set", "disabled", false)
