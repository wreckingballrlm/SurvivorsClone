extends Area2D


signal hurt(damage: float, angle: Vector2, knockback_amount: float)


@export_enum("COOLDOWN", "HIT_ONCE", "DISABLE_HIT_BOX") var hurt_box_type: int = 0

var hit_once_array: Array = []

@onready var collision_shape_2d: CollisionShape2D = %CollisionShape2D
@onready var disable_timer: Timer = %DisableTimer


func _on_area_entered(area: Area2D) -> void:
	if area.is_in_group("attack"):
		if not area.get("damage") == null:
			match hurt_box_type:
				0: # COOLDOWN
					collision_shape_2d.call_deferred("set", "disabled", true)
					disable_timer.start()
				1: # HIT_ONCE
					if not hit_once_array.has(area):
						hit_once_array.append(area)
						if area.has_signal("removed_from_array"):
							if not area.is_connected("removed_from_array", Callable(self, "remove_from_list")):
								area.connect("removed_from_array", Callable(self, "remove_from_list"))
					else:
						return
				2: # DISABLE_HIT_BOX
					if area.has_method("temp_disable"):
						area.temp_disable()
			var damage: float = area.damage
			var angle := Vector2.ZERO
			var knockback_amount: float = 1.0
			if area.get("angle"):
				angle = area.angle
			if area.get("knockback_amount"):
				knockback_amount = area.knockback_amount
			
			hurt.emit(damage, angle, knockback_amount)
			if area.has_method("enemy_hit"): # Do I like hurtboxes reaching into attacks?
				area.enemy_hit(1)


func _on_disable_timer_timeout() -> void:
	collision_shape_2d.call_deferred("set", "disabled", false)


func remove_from_list(object) -> void:
	if hit_once_array.has(object):
		hit_once_array.erase(object)
