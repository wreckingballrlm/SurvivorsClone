extends CharacterBody2D


signal removed_from_array(object)


@export var movement_speed: float = 20.0
@export var hp: float = 10.0
@export var knockback_recovery: float = 3.5
@export var experience_value: int = 1

var flip_threshold: float = 0.1
var knockback := Vector2.ZERO
var death_animation := preload("res://enemy/explosion.tscn")
var exp_gem := preload("res://objects/experience_gem.tscn")

@onready var player := get_tree().get_first_node_in_group("player")
@onready var loot_base := get_tree().get_first_node_in_group("loot")
@onready var sprite_2d: Sprite2D = %Sprite2D
@onready var hit_sound_player_2d: AudioStreamPlayer2D = %HitSoundPlayer2D


func _physics_process(delta: float) -> void:
	knockback = knockback.move_toward(Vector2.ZERO, knockback_recovery)
	var direction: Vector2 = global_position.direction_to(player.global_position)
	velocity = direction * movement_speed
	velocity += knockback
	move_and_slide()
	
	if direction.x > flip_threshold:
		sprite_2d.flip_h = true
	elif direction.x < -flip_threshold:
		sprite_2d.flip_h = false


func _on_hurt_box_hurt(damage: float, angle: Vector2, knockback_amount: float) -> void:
	hp -= damage
	knockback = angle * knockback_amount
	if hp <= 0:
		die()
	else:
		hit_sound_player_2d.play()


func die() -> void:
	removed_from_array.emit(self)
	var enemy_death := death_animation.instantiate()
#	enemy_death.scale = sprite_2d.scale
	enemy_death.global_position = global_position
	get_parent().call_deferred("add_child", enemy_death)
	spawn_gem()
	queue_free()


func spawn_gem() -> void:
	var new_gem = exp_gem.instantiate()
	new_gem.global_position = global_position
	new_gem.experience_value = experience_value
	loot_base.call_deferred("add_child", new_gem)
