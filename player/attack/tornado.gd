class_name Tornado
extends "res://player/attack/attack.gd"


var last_movement := Vector2.ZERO
var angle_less := Vector2.ZERO
var angle_more := Vector2.ZERO
var angle_factor: float = 500.0


func _ready() -> void:
	match level:
		1:
			hp = 9999
			speed = 100
			damage = 5
			knockback_amount = 100
			attack_size = 1.0 * (1.0 + player.spell_size)
		2:
			hp = 9999
			speed = 100
			damage = 5
			knockback_amount = 100
			attack_size = 1.0 * (1.0 + player.spell_size)
		3:
			hp = 9999
			speed = 100
			damage = 5
			knockback_amount = 100
			attack_size = 1.0 * (1.0 + player.spell_size)
		4:
			hp = 9999
			speed = 100
			damage = 5
			knockback_amount = 125
			attack_size = 1.0 * (1.0 + player.spell_size)
	var move_to_less := Vector2.ZERO
	var move_to_more := Vector2.ZERO
	match last_movement:
		Vector2.UP, Vector2.DOWN:
			move_to_less = global_position + Vector2(randf_range(-1.0, -0.25), last_movement.y) * angle_factor
			move_to_more = global_position + Vector2(randf_range(0.25, 1), last_movement.y) * angle_factor
		Vector2.LEFT, Vector2.RIGHT:
			move_to_less = global_position + Vector2(last_movement.x, randf_range(-1.0, -0.25)) * angle_factor
			move_to_more = global_position + Vector2(last_movement.x, randf_range(0.25, 1)) * angle_factor
		Vector2(1, 1), Vector2(-1, -1), Vector2(1, -1), Vector2(-1, 1):
			move_to_less = global_position + Vector2(last_movement.x, last_movement.y * randf_range(0, 0.75)) * angle_factor
			move_to_more = global_position + Vector2(last_movement.x * randf_range(0, 0.75), last_movement.y) * angle_factor
	
	angle_less = global_position.direction_to(move_to_less)
	angle_more = global_position.direction_to(move_to_more)
	
	tween_scale(3.0)
	tween_speed(6.0, speed)
	tween_angle(2.0, 3, angle_less, angle_more)
