extends Attack


@export var paths: int = 1
@export var attack_speed: float = 4.0

var target_array: Array = []
var reset_position := Vector2.ZERO
var reset_position_offset: float = 50.0

var javelin_sprite := preload("res://assets/Textures/Items/Weapons/javelin_3_new.png")
var javelin_attack_sprite := preload("res://assets/Textures/Items/Weapons/javelin_3_new_attack.png")

@onready var attack_timer: Timer = %AttackTimer
@onready var change_direction_timer: Timer = %ChangeDirectionTimer
@onready var reset_position_timer: Timer = %ResetPositionTimer
@onready var shoot_sound_player_2d: AudioStreamPlayer2D = %ShootSoundPlayer2D


func _ready() -> void:
	set_reset_position()
	update_javelin()


func _physics_process(delta: float) -> void:
	if target_array.size() > 0:
		position += angle * speed * delta
	else:
		var player_angle: Vector2 = global_position.direction_to(reset_position)
		var distance_diff: Vector2 = global_position - player.global_position
		var return_speed: float = 20.0
		if abs(distance_diff.x) > 500 or abs(distance_diff.y) > 500:
			return_speed = 100
		position += player_angle * return_speed * delta
		rotation = global_position.direction_to(player.global_position).angle() * deg_to_rad(135) # Magic Number


func update_javelin() -> void:
	level = player.javelin_level
	match level:
		1:
			hp = 9999
			speed = 200
			damage = 10
			knockback_amount = 100
			paths = 1
			attack_size = 1 * (1.0 + player.spell_size)
			attack_speed = 5 * (1.0 - player.spell_cooldown)
		2:
			hp = 9999
			speed = 200
			damage = 10
			knockback_amount = 100
			paths = 2
			attack_size = 1 * (1.0 + player.spell_size)
			attack_speed = 5 * (1.0 - player.spell_cooldown)
		3:
			hp = 9999
			speed = 200
			damage = 10
			knockback_amount = 100
			paths = 3
			attack_size = 1 * (1.0 + player.spell_size)
			attack_speed = 5 * (1.0 - player.spell_cooldown)
		4:
			hp = 9999
			speed = 200
			damage = 15
			knockback_amount = 120
			paths = 3
			attack_size = 1 * (1.0 + player.spell_size)
			attack_speed = 5 * (1.0 - player.spell_cooldown)
	scale = Vector2(1.0, 1.0) * attack_size
	attack_timer.wait_time = attack_speed


func _on_attack_timer_timeout() -> void:
	add_paths()


func add_paths() -> void:
	shoot_sound_player_2d.play()
	removed_from_array.emit(self)
	target_array.clear()
	var counter: int = 0
	while counter < paths:
		var new_path = player.get_random_target()
		target_array.append(new_path)
		counter += 1
		enable_attack(true)
	target = target_array[0]
	process_path()


func process_path() -> void:
	angle = global_position.direction_to(target)
	change_direction_timer.start()
	
	tween_rotation(0.25, 135)


func _on_change_direction_timer_timeout() -> void:
	if target_array.size() > 0:
		target_array.remove_at(0)
		if target_array.size() > 0:
			target = target_array[0]
			process_path()
			shoot_sound_player_2d.play()
			removed_from_array.emit(self)
		else:
			enable_attack(false)
	else:
		change_direction_timer.stop()
		attack_timer.start()
		enable_attack(false)


func enable_attack(attack: bool = true) -> void:
	if attack:
		collision_shape_2d.set_deferred("disabled", false)
		sprite_2d.texture = javelin_attack_sprite
	else:
		collision_shape_2d.set_deferred("disabled", true)
		sprite_2d.texture = javelin_sprite


func _on_reset_position_timer_timeout() -> void:
	set_reset_position()


func set_reset_position() -> void:
	var choose_direction: int = randi() % 4
	reset_position = player.global_position
	match choose_direction:
		0:
			reset_position.x += reset_position_offset
		1:
			reset_position.x -= reset_position_offset
		2:
			reset_position.y += reset_position_offset
		3:
			reset_position.y -= reset_position_offset
