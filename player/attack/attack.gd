class_name Attack
extends Area2D


signal removed_from_array(object)


@export var lifetime: float = 10.0

@export var start_level: int = 1
@export var start_hp: float = 2.0
@export var start_speed: float = 100.0
@export var start_damage: float = 5.0
@export var start_knockback_amount: float = 100.0
@export var start_attack_size: float = 1.0

var target := Vector2.ZERO
var angle := Vector2.ZERO
var is_dead: bool = false

@onready var level: int = start_level
@onready var hp: float = start_hp
@onready var speed: float = start_speed
@onready var damage: float = start_damage
@onready var knockback_amount: float = start_knockback_amount
@onready var attack_size: float = start_attack_size

@onready var player := get_tree().get_first_node_in_group("player")
@onready var lifetime_timer: Timer = %LifetimeTimer
@onready var collision_shape_2d: CollisionShape2D = %CollisionShape2D
@onready var shoot_sound_player: AudioStreamPlayer = %ShootSoundPlayer
@onready var sprite_2d: Sprite2D = %Sprite2D


func _physics_process(delta: float) -> void:
	position += angle * speed * delta
	if is_dead and not shoot_sound_player.playing:
		queue_free()


func enemy_hit(charge: float) -> void:
	hp -= charge
	if hp <= 0:
		die()


func _on_lifetime_timer_timeout() -> void:
	die()


func die() -> void:
	removed_from_array.emit(self)
	collision_shape_2d.set_deferred("disabled", true)
	visible = false
	is_dead = true


func _on_shoot_sound_player_finished() -> void:
	if is_dead:
		queue_free()


func tween_scale(tween_duration: float) -> void:
	var tween = create_tween().set_parallel(true)
	tween.tween_property(self, "scale", Vector2(1, 1) * attack_size, tween_duration).set_trans(Tween.TRANS_QUINT).set_ease(Tween.EASE_OUT)
	tween.play()


func tween_angle(tween_duration: float, number_of_loops: int, angle_less: Vector2, angle_more: Vector2) -> void:
	var tween = create_tween().set_parallel(true)
	tween.set_loops(number_of_loops)
	var set_angle = randi_range(0, 1)
	if set_angle:
		angle = angle_less
		tween.tween_property(self, "angle", angle_more, tween_duration)
		tween.tween_property(self, "angle", angle_less, tween_duration)
	else:
		angle = angle_more
		tween.tween_property(self, "angle", angle_less, tween_duration)
		tween.tween_property(self, "angle", angle_less, tween_duration)
	tween.play()


func tween_speed(tween_duration: float, final_speed: float) -> void:
	var tween = create_tween().set_parallel(true)
	speed = speed / 5.0
	tween.tween_property(self, "speed", final_speed, tween_duration).set_trans(Tween.TRANS_QUINT).set_ease(Tween.EASE_OUT)
	tween.play()


func tween_rotation(tween_duration: float, rotate_by: float) -> void:
	var tween = create_tween()
	var new_rotation_degrees: float = angle.angle() + deg_to_rad(rotate_by)
	tween.tween_property(self, "rotation", new_rotation_degrees, tween_duration).set_trans(Tween.TRANS_QUINT).set_ease(Tween.EASE_OUT)
	tween.play()
