class_name IceSpear
extends "res://player/attack/attack.gd"


func _ready() -> void:
	scale = Vector2(0.1, 0.1)
	angle = global_position.direction_to(target)
	rotation = angle.angle() + deg_to_rad(135)
	match level: # I want to do this in a more customizable way in the inspector. Involves working with UpgradeDB and attack base class I think
		1:
			hp = 1
			speed = 100
			damage = 5
			knockback_amount = 100
			attack_size = 1.0 * (1 + player.spell_size)
		2:
			hp = 1
			speed = 100
			damage = 5
			knockback_amount = 100
			attack_size = 1.0 * (1 + player.spell_size)
		3:
			hp = 2
			speed = 100
			damage = 8
			knockback_amount = 100
			attack_size = 1.0 * (1 + player.spell_size)
		4:
			hp = 2
			speed = 100
			damage = 8
			knockback_amount = 100
			attack_size = 1.0 * (1 + player.spell_size)
	
	tween_scale(1.0)
