extends CharacterBody2D


@export var movement_speed: float = 40.0
@export var hp: float = 20.0
@export var max_hp: float = 80.0

var last_movement := Vector2.UP

var experience: int = 0
var experience_level: int = 1
var collected_experience: int = 0

# Attack
var ice_spear := preload("res://player/attack/ice_spear.tscn")
var tornado := preload("res://player/attack/tornado.tscn")
var javelin := preload("res://player/attack/javelin.tscn")

# Ice Spear
var ice_spear_ammo: int = 0
var ice_spear_base_ammo: int = 0
var ice_spear_attack_speed: float = 1.5
var ice_spear_level: int = 0

# Tornado
var tornado_ammo: int = 0
var tornado_base_ammo: int = 0
var tornado_attack_speed: float = 3.0
var tornado_level: int = 0

# Javelin
var javelin_ammo: int = 0
var javelin_level: int = 0

# Enemy-Related
var enemy_close: Array = []

# Attack Nodes
@onready var ice_spear_timer: Timer = %IceSpearTimer
@onready var ice_spear_attack_timer: Timer = %IceSpearAttackTimer
@onready var tornado_timer: Timer = %TornadoTimer
@onready var tornado_attack_timer: Timer = %TornadoAttackTimer
@onready var javelin_base: Node2D = %JavelinBase

# Node references
@onready var sprite_2d: Sprite2D = %Sprite2D
@onready var walk_timer: Timer = %WalkTimer
@onready var attack_container: Node = %AttackContainer
@onready var experience_bar: TextureProgressBar = %ExperienceBar
@onready var level_label: Label = %LevelLabel
@onready var level_up_panel: Panel = %LevelUp
@onready var level_up_label: Label = %LevelUpLabel
@onready var upgrades_container: VBoxContainer = %UpgradeOptions
@onready var item_options = preload("res://utility/item_option.tscn")
@onready var level_up_sound_player: AudioStreamPlayer = %LevelUpSoundPlayer

# Upgrades
var collected_upgrades: Array = []
var upgrade_options: Array = []
var armor = 0
var speed = 0
var spell_cooldown = 0
var spell_size = 0
var additional_attacks = 0


func _ready() -> void:
	upgrade_character("ice_spear1")
	attack()
	set_experience_bar(experience, calculate_experience_cap())


func _physics_process(delta: float) -> void:
	var movement_vector: Vector2 = get_movement()
	velocity = movement_vector.normalized() * movement_speed
	if movement_vector != Vector2.ZERO:
		set_last_movement(movement_vector)
		animate_walk()
	move_and_slide()


func get_movement() -> Vector2:
	var x_movement: float = Input.get_axis("left", "right")
	var y_movement: float = Input.get_axis("up", "down")
	var movement_vector := Vector2(x_movement, y_movement)
	
	if movement_vector.x > 0:
		sprite_2d.flip_h = true
	elif movement_vector.x < 0:
		sprite_2d.flip_h = false
		
	return movement_vector


func animate_walk() -> void:
	if walk_timer.is_stopped():
		if sprite_2d.frame >= sprite_2d.hframes - 1:
			sprite_2d.frame = 0
		else:
			sprite_2d.frame += 1
		walk_timer.start()


func _on_hurt_box_hurt(damage: float, _angle: Vector2, _knockback_amount: float) -> void:
	hp -= clamp(damage - armor, 1.0, 999.0)
	print(hp)


func attack() -> void:
	if ice_spear_level > 0:
		ice_spear_timer.wait_time = ice_spear_attack_speed * (1 - spell_cooldown)
		if ice_spear_timer.is_stopped():
			ice_spear_timer.start()
	if tornado_level > 0:
		tornado_timer.wait_time = tornado_attack_speed
		if tornado_timer.is_stopped():
			tornado_timer.start()
	if javelin_level > 0:
		spawn_javelin()


func _on_ice_spear_timer_timeout() -> void:
	ice_spear_ammo += ice_spear_base_ammo + additional_attacks
	ice_spear_attack_timer.start()


func _on_ice_spear_attack_timer_timeout() -> void:
	if ice_spear_ammo > 0:
		var ice_spear_attack = instantiate_ice_spear()
		attack_container.add_child(ice_spear_attack) # Is this necessary or not?
		ice_spear_ammo -= 1
		if ice_spear_ammo > 0:
			ice_spear_attack_timer.start()
		else:
			ice_spear_attack_timer.stop()


func instantiate_ice_spear() -> IceSpear:
	var ice_spear_attack := ice_spear.instantiate()
	ice_spear_attack.global_position = global_position
	ice_spear_attack.target = get_random_target()
	ice_spear_attack.level = ice_spear_level
	return ice_spear_attack


func _on_tornado_timer_timeout() -> void:
	tornado_ammo += tornado_base_ammo + additional_attacks
	tornado_attack_timer.start()


func _on_tornado_attack_timer_timeout() -> void:
	if tornado_ammo > 0:
		var tornado_attack = instantiate_tornado()
		attack_container.add_child(tornado_attack) # Is this necessary or not?
		tornado_ammo -= 1
		if tornado_ammo > 0:
			tornado_attack_timer.start()
		else:
			tornado_attack_timer.stop()


func instantiate_tornado() -> Tornado:
	var tornado_attack := tornado.instantiate()
	tornado_attack.last_movement = last_movement
	tornado_attack.global_position = global_position
	tornado_attack.level = tornado_level
	return tornado_attack


func spawn_javelin() -> void:
	var get_javelin_total: int = javelin_base.get_child_count()
	var calc_spawns: int = (javelin_ammo + additional_attacks) - get_javelin_total
	while calc_spawns > 0:
		var javelin_spawn := javelin.instantiate()
		javelin_spawn.global_position = global_position
		javelin_base.add_child(javelin_spawn)
		calc_spawns -= 1
	# Update javelin
	var javelins = javelin_base.get_children()
	for i in javelins:
		if i.has_method("update_javelin"):
			i.update_javelin()


func get_random_target() -> Vector2:
	if enemy_close.size():
		return enemy_close.pick_random().global_position
	else:
		return Vector2.UP


func _on_enemy_detection_area_body_entered(body: Node2D) -> void:
	if not enemy_close.has(body):
		enemy_close.append(body)


func _on_enemy_detection_area_body_exited(body: Node2D) -> void:
	if enemy_close.has(body):
		enemy_close.erase(body)


func set_last_movement(movement: Vector2) -> void:
	last_movement = movement


func _on_grab_area_area_entered(area: Area2D) -> void:
	if area.is_in_group("loot"):
		area.target = self


func _on_collect_area_area_entered(area: Area2D) -> void:
	if area.is_in_group("loot"):
		calculate_experience(area.collect())


func calculate_experience(gem_exp: int) -> void:
	var exp_required = calculate_experience_cap()
	collected_experience += gem_exp
	if experience + collected_experience >= exp_required:
		collected_experience -= exp_required + experience
		experience_level += 1
		level_label.text = "Level: " + str(experience_level)
		print("Level: " + str(experience_level))
		experience = 0
		exp_required = calculate_experience_cap()
		level_up()
	else:
		experience += collected_experience
		collected_experience = 0
	set_experience_bar(experience, exp_required)


func calculate_experience_cap() -> int: # Just full of magic numbers
	var exp_cap: int = experience_level
	if experience_level < 20:
		exp_cap = experience_level * 5
	elif experience_level < 40:
		exp_cap += 95 * (experience_level - 19) * 8
	else:
		exp_cap = 255 + (experience_level - 39) * 12
	return exp_cap


func set_experience_bar(value: int = 1, max_value: int = 100):
	experience_bar.value = value
	experience_bar.max_value = max_value


func level_up() -> void:
	level_up_sound_player.play()
	var tween = level_up_panel.create_tween()
	tween.tween_property(level_up_panel, "position", Vector2(220, 50), 0.2).set_trans(Tween.TRANS_QUINT).set_ease(Tween.EASE_IN) # Magic Number
	tween.play()
	level_up_panel.set_deferred("visible", true)
	get_tree().set_deferred("paused", true)
	var options: int = 0 # Magic Number
	var options_max: int = 3 # Magic Number
	while options < options_max:
		var option = item_options.instantiate()
		option.item = get_random_item()
		upgrades_container.call_deferred("add_child", option)
		options += 1


func upgrade_character(upgrade) -> void:
	match upgrade:
		"ice_spear1":
			ice_spear_level = 1
			ice_spear_base_ammo += 1
		"ice_spear2":
			ice_spear_level = 2
			ice_spear_base_ammo += 1
		"icespear3":
			ice_spear_level = 3
		"icespear4":
			ice_spear_level = 4
			ice_spear_base_ammo += 2
		"tornado1":
			tornado_level = 1
			tornado_base_ammo += 1
		"tornado2":
			tornado_level = 2
			tornado_base_ammo += 1
		"tornado3":
			tornado_level = 3
			tornado_attack_speed -= 0.5
		"tornado4":
			tornado_level = 4
			tornado_base_ammo += 1
		"javelin1":
			javelin_level = 1
			javelin_ammo = 1
		"javelin2":
			javelin_level = 2
		"javelin3":
			javelin_level = 3
		"javelin4":
			javelin_level = 4
		"armor1","armor2","armor3","armor4":
			armor += 1
		"speed1","speed2","speed3","speed4":
			movement_speed += 20.0
		"tome1","tome2","tome3","tome4":
			spell_size += 0.10
		"scroll1","scroll2","scroll3","scroll4":
			spell_cooldown += 0.05
		"ring1","ring2":
			additional_attacks += 1
		"food":
			hp += 20
			hp = clamp(hp,0,max_hp)

	attack()
	var option_children := upgrades_container.get_children()
	for i in option_children:
		i.queue_free()
	upgrade_options.clear()
	collected_upgrades.append(upgrade)
	level_up_panel.set_deferred("visible", false)
	level_up_panel.set_deferred("position", Vector2(800, 50))
	get_tree().set_deferred("paused", false)
	calculate_experience(0)


func get_random_item():
	var db_list: Array = []
	for i in UpgradeDB.UPGRADES:
		if i in collected_upgrades: # Find already collected upgrades
			pass
		elif i in upgrade_options: # If the upgrade is already an option
			pass
		elif UpgradeDB.UPGRADES[i]["type"] == "item": # Don't pick food
			pass
		elif UpgradeDB.UPGRADES[i]["prerequisite"].size() > 0: # Check for prereqs
			var to_add: bool = true
			for n in UpgradeDB.UPGRADES[i]["prerequisite"]:
				if not n in collected_upgrades: # If have not collected prereq
					to_add = false
			if to_add:
				db_list.append(i)
		else:
			db_list.append(i)
	if db_list.size() > 0:
		var random_item = db_list.pick_random()
		upgrade_options.append(random_item)
		return random_item
	else:
		return null


func _on_upgrade_options_child_entered_tree(node: Node) -> void:
	if node.has_signal("pressed") and not node.pressed.is_connected(upgrade_character):
		node.pressed.connect(upgrade_character.bind(node.item)) # Change argument to real upgrade next tutorial
